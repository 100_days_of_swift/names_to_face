//
//  Person.swift
//  NamesToFace
//
//  Created by Hariharan S on 25/05/24.
//

import Foundation

class Person: NSObject {
    var name: String
    var image: String
    
    init(
        name: String,
        image: String
    ) {
        self.name = name
        self.image = image
    }
}
