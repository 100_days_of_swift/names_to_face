//
//  PersonCell.swift
//  NamesToFace
//
//  Created by Hariharan S on 24/05/24.
//

import UIKit

class PersonCell: UICollectionViewCell {
    
    // MARK: - IBOutlets

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var name: UILabel!
}
