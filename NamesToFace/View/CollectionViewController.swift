//
//  CollectionViewController.swift
//  NamesToFace
//
//  Created by Hariharan S on 24/05/24.
//

import UIKit

class CollectionViewController: UICollectionViewController {

    // MARK: - Property

    var people = [Person]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Persons"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(self.addNewPerson)
        )
    }
}

// MARK: - Private Methods

private extension CollectionViewController {
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(
            for: .documentDirectory,
            in: .userDomainMask
        )
        return paths[0]
    }
}

// MARK: - UICollectionViewDataSource Conformance

extension CollectionViewController {
    override func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        return self.people.count
    }

    override func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: "Person",
            for: indexPath
        ) as? PersonCell
        else {
            fatalError("Unable to dequeue PersonCell.")
        }
        
        let person = self.people[indexPath.item]
        cell.name.text = person.name
        let path = self.getDocumentsDirectory().appendingPathComponent(person.image)
        cell.imageView.image = UIImage(contentsOfFile: path.path)
        cell.imageView.layer.borderColor = UIColor(white: 0, alpha: 0.3).cgColor
        cell.imageView.layer.borderWidth = 2
        cell.imageView.layer.cornerRadius = 3
        cell.layer.cornerRadius = 7
        return cell
    }
}
// MARK: - UICollectionViewDelegate Conformance

extension CollectionViewController {
    override func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        let person = self.people[indexPath.item]
        let ac = UIAlertController(
            title: "Rename person",
            message: nil, 
            preferredStyle: .alert
        )
        ac.addTextField()
        ac.addAction(
            UIAlertAction(
                title: "Cancel",
                style: .cancel
            )
        )
        ac.addAction(
            UIAlertAction(
                title: "OK", 
                style: .default
            ) { [weak self, weak ac] _ in
                guard let newName = ac?.textFields?[0].text
                else {
                    return
                }
                person.name = newName
                self?.collectionView.reloadData()
            }
        )
        self.present(ac, animated: true)
    }
}
// MARK: - UINavigationControllerDelegate Conformance

extension CollectionViewController: UINavigationControllerDelegate {
    
}

// MARK: - UIImagePickerControllerDelegate Conformance

extension CollectionViewController: UIImagePickerControllerDelegate {
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]
    ) {
        guard let image = info[.editedImage] as? UIImage
        else {
            return
        }
        
        let imageName = UUID().uuidString
        let imagePath = self.getDocumentsDirectory().appendingPathComponent(imageName)
        
        if let jpegData = image.jpegData(compressionQuality: 0.8) {
            try? jpegData.write(to: imagePath)
        }
        
        let person = Person(name: "Unknown", image: imageName)
        self.people.append(person)
        self.collectionView.reloadData()
        
        self.dismiss(animated: true)
    }
}

// MARK: - Objc Methods

@objc
private extension CollectionViewController {
    func addNewPerson() {
       let picker = UIImagePickerController()
       picker.allowsEditing = true
       picker.delegate = self
       present(picker, animated: true)
   }
}
